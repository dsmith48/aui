'''A custom module for event declarations, so as to similify code and remove
circular references'''

import wx


# AuiToolBar events
wxEVT_COMMAND_AUITOOLBAR_TOOL_DROPDOWN = wx.NewEventType()
wxEVT_COMMAND_AUITOOLBAR_OVERFLOW_CLICK = wx.NewEventType()
wxEVT_COMMAND_AUITOOLBAR_RIGHT_CLICK = wx.NewEventType()
wxEVT_COMMAND_AUITOOLBAR_MIDDLE_CLICK = wx.NewEventType()
wxEVT_COMMAND_AUITOOLBAR_BEGIN_DRAG = wx.NewEventType()

EVT_AUITOOLBAR_TOOL_DROPDOWN = \
    wx.PyEventBinder(wxEVT_COMMAND_AUITOOLBAR_TOOL_DROPDOWN, 1)
""" A dropdown `AuiToolBarItem` is being shown. """
EVT_AUITOOLBAR_OVERFLOW_CLICK = \
    wx.PyEventBinder(wxEVT_COMMAND_AUITOOLBAR_OVERFLOW_CLICK, 1)
""" The user left-clicked on the overflow button in `AuiToolBar`. """
EVT_AUITOOLBAR_RIGHT_CLICK = \
    wx.PyEventBinder(wxEVT_COMMAND_AUITOOLBAR_RIGHT_CLICK, 1)
""" Fires an event when the user right-clicks on a `AuiToolBarItem`. """
EVT_AUITOOLBAR_MIDDLE_CLICK = \
    wx.PyEventBinder(wxEVT_COMMAND_AUITOOLBAR_MIDDLE_CLICK, 1)
""" Fires an event when the user middle-clicks on a `AuiToolBarItem`. """
EVT_AUITOOLBAR_BEGIN_DRAG = \
    wx.PyEventBinder(wxEVT_COMMAND_AUITOOLBAR_BEGIN_DRAG, 1)
""" A drag operation involving a toolbar item has started. """


# AuiNotebook events
wxEVT_COMMAND_AUINOTEBOOK_PAGE_CLOSE = wx.NewEventType()
wxEVT_COMMAND_AUINOTEBOOK_PAGE_CLOSED = wx.NewEventType()
wxEVT_COMMAND_AUINOTEBOOK_PAGE_CHANGED = wx.NewEventType()
wxEVT_COMMAND_AUINOTEBOOK_PAGE_CHANGING = wx.NewEventType()
wxEVT_COMMAND_AUINOTEBOOK_BUTTON = wx.NewEventType()
wxEVT_COMMAND_AUINOTEBOOK_BEGIN_DRAG = wx.NewEventType()
wxEVT_COMMAND_AUINOTEBOOK_END_DRAG = wx.NewEventType()
wxEVT_COMMAND_AUINOTEBOOK_DRAG_MOTION = wx.NewEventType()
wxEVT_COMMAND_AUINOTEBOOK_ALLOW_DND = wx.NewEventType()
wxEVT_COMMAND_AUINOTEBOOK_DRAG_DONE = wx.NewEventType()
wxEVT_COMMAND_AUINOTEBOOK_TAB_LEFT_UP = wx.NewEventType()
wxEVT_COMMAND_AUINOTEBOOK_TAB_MIDDLE_DOWN = wx.NewEventType()
wxEVT_COMMAND_AUINOTEBOOK_TAB_MIDDLE_UP = wx.NewEventType()
wxEVT_COMMAND_AUINOTEBOOK_TAB_RIGHT_DOWN = wx.NewEventType()
wxEVT_COMMAND_AUINOTEBOOK_TAB_RIGHT_UP = wx.NewEventType()
wxEVT_COMMAND_AUINOTEBOOK_TAB_DCLICK = wx.NewEventType()
wxEVT_COMMAND_AUINOTEBOOK_BG_LEFT_UP = wx.NewEventType()
wxEVT_COMMAND_AUINOTEBOOK_BG_MIDDLE_DOWN = wx.NewEventType()
wxEVT_COMMAND_AUINOTEBOOK_BG_MIDDLE_UP = wx.NewEventType()
wxEVT_COMMAND_AUINOTEBOOK_BG_RIGHT_DOWN = wx.NewEventType()
wxEVT_COMMAND_AUINOTEBOOK_BG_RIGHT_UP = wx.NewEventType()
wxEVT_COMMAND_AUINOTEBOOK_BG_DCLICK = wx.NewEventType()

# Define a new event for a drag cancelled
wxEVT_COMMAND_AUINOTEBOOK_CANCEL_DRAG = wx.NewEventType()

# Define events for editing a tab label
wxEVT_COMMAND_AUINOTEBOOK_BEGIN_LABEL_EDIT = wx.NewEventType()
wxEVT_COMMAND_AUINOTEBOOK_END_LABEL_EDIT = wx.NewEventType()

# Create event binders
EVT_AUINOTEBOOK_PAGE_CLOSE = \
    wx.PyEventBinder(wxEVT_COMMAND_AUINOTEBOOK_PAGE_CLOSE, 1)
""" A tab in `AuiNotebook` is being closed. Can be vetoed by
calling `Veto()`. """
EVT_AUINOTEBOOK_PAGE_CLOSED = \
    wx.PyEventBinder(wxEVT_COMMAND_AUINOTEBOOK_PAGE_CLOSED, 1)
""" A tab in `AuiNotebook` has been closed. """
EVT_AUINOTEBOOK_PAGE_CHANGED = \
    wx.PyEventBinder(wxEVT_COMMAND_AUINOTEBOOK_PAGE_CHANGED, 1)
""" The page selection was changed. """
EVT_AUINOTEBOOK_PAGE_CHANGING = \
    wx.PyEventBinder(wxEVT_COMMAND_AUINOTEBOOK_PAGE_CHANGING, 1)
""" The page selection is being changed. """
EVT_AUINOTEBOOK_BUTTON = \
    wx.PyEventBinder(wxEVT_COMMAND_AUINOTEBOOK_BUTTON, 1)
""" The user clicked on a button in the `AuiNotebook` tab area. """
EVT_AUINOTEBOOK_BEGIN_DRAG = \
    wx.PyEventBinder(wxEVT_COMMAND_AUINOTEBOOK_BEGIN_DRAG, 1)
""" A drag-and-drop operation on a notebook tab has started. """
EVT_AUINOTEBOOK_END_DRAG = \
    wx.PyEventBinder(wxEVT_COMMAND_AUINOTEBOOK_END_DRAG, 1)
""" A drag-and-drop operation on a notebook tab has finished. """
EVT_AUINOTEBOOK_DRAG_MOTION = \
    wx.PyEventBinder(wxEVT_COMMAND_AUINOTEBOOK_DRAG_MOTION, 1)
""" A drag-and-drop operation on a notebook tab is ongoing. """
EVT_AUINOTEBOOK_ALLOW_DND = \
    wx.PyEventBinder(wxEVT_COMMAND_AUINOTEBOOK_ALLOW_DND, 1)
""" Fires an event asking if it is OK to drag and drop a tab. """
EVT_AUINOTEBOOK_DRAG_DONE = \
    wx.PyEventBinder(wxEVT_COMMAND_AUINOTEBOOK_DRAG_DONE, 1)
""" A drag-and-drop operation on a notebook tab has finished. """
EVT_AUINOTEBOOK_TAB_LEFT_UP = \
    wx.PyEventBinder(wxEVT_COMMAND_AUINOTEBOOK_TAB_LEFT_UP, 1)
""" The user clicked with the left mouse button on a tab. """
EVT_AUINOTEBOOK_TAB_MIDDLE_DOWN = \
    wx.PyEventBinder(wxEVT_COMMAND_AUINOTEBOOK_TAB_MIDDLE_DOWN, 1)
""" The user clicked with the middle mouse button on a tab. """
EVT_AUINOTEBOOK_TAB_MIDDLE_UP = \
    wx.PyEventBinder(wxEVT_COMMAND_AUINOTEBOOK_TAB_MIDDLE_UP, 1)
""" The user clicked with the middle mouse button on a tab. """
EVT_AUINOTEBOOK_TAB_RIGHT_DOWN = \
    wx.PyEventBinder(wxEVT_COMMAND_AUINOTEBOOK_TAB_RIGHT_DOWN, 1)
""" The user clicked with the right mouse button on a tab. """
EVT_AUINOTEBOOK_TAB_RIGHT_UP = \
    wx.PyEventBinder(wxEVT_COMMAND_AUINOTEBOOK_TAB_RIGHT_UP, 1)
""" The user clicked with the right mouse button on a tab. """
EVT_AUINOTEBOOK_BG_LEFT_UP = \
    wx.PyEventBinder(wxEVT_COMMAND_AUINOTEBOOK_BG_LEFT_UP, 1)
""" The user left-clicked in the tab area but not over a tab or a button. """
EVT_AUINOTEBOOK_BG_MIDDLE_DOWN = \
    wx.PyEventBinder(wxEVT_COMMAND_AUINOTEBOOK_BG_MIDDLE_DOWN, 1)
""" The user middle-clicked in the tab area but not over a tab or a button. """
EVT_AUINOTEBOOK_BG_MIDDLE_UP = \
    wx.PyEventBinder(wxEVT_COMMAND_AUINOTEBOOK_BG_MIDDLE_UP, 1)
""" The user middle-clicked in the tab area but not over a tab or a button. """
EVT_AUINOTEBOOK_BG_RIGHT_DOWN = \
    wx.PyEventBinder(wxEVT_COMMAND_AUINOTEBOOK_BG_RIGHT_DOWN, 1)
""" The user right-clicked in the tab area but not over a tab or a button. """
EVT_AUINOTEBOOK_BG_RIGHT_UP = \
    wx.PyEventBinder(wxEVT_COMMAND_AUINOTEBOOK_BG_RIGHT_UP, 1)
""" The user right-clicked in the tab area but not over a tab or a button. """
EVT_AUINOTEBOOK_BG_DCLICK = \
    wx.PyEventBinder(wxEVT_COMMAND_AUINOTEBOOK_BG_DCLICK, 1)
""" The user left-clicked on the tab area not occupied by
`AuiNotebook` tabs. """
EVT_AUINOTEBOOK_CANCEL_DRAG = \
    wx.PyEventBinder(wxEVT_COMMAND_AUINOTEBOOK_CANCEL_DRAG, 1)
""" A drag and drop operation has been cancelled. """
EVT_AUINOTEBOOK_TAB_DCLICK = \
    wx.PyEventBinder(wxEVT_COMMAND_AUINOTEBOOK_TAB_DCLICK, 1)
""" The user double-clicked with the left mouse button on a tab. """
EVT_AUINOTEBOOK_BEGIN_LABEL_EDIT = \
    wx.PyEventBinder(wxEVT_COMMAND_AUINOTEBOOK_BEGIN_LABEL_EDIT, 1)
""" The user double-clicked with the left mouse button on a tab which
text is editable. """
EVT_AUINOTEBOOK_END_LABEL_EDIT = \
    wx.PyEventBinder(wxEVT_COMMAND_AUINOTEBOOK_END_LABEL_EDIT, 1)
""" The user finished editing a tab label. """


# AUI Events (From FrameManager)
wxEVT_AUI_PANE_BUTTON = wx.NewEventType()
wxEVT_AUI_PANE_CLOSE = wx.NewEventType()
wxEVT_AUI_PANE_MAXIMIZE = wx.NewEventType()
wxEVT_AUI_PANE_RESTORE = wx.NewEventType()
wxEVT_AUI_RENDER = wx.NewEventType()
wxEVT_AUI_FIND_MANAGER = wx.NewEventType()
wxEVT_AUI_PANE_MINIMIZE = wx.NewEventType()
wxEVT_AUI_PANE_MIN_RESTORE = wx.NewEventType()
wxEVT_AUI_PANE_FLOATING = wx.NewEventType()
wxEVT_AUI_PANE_FLOATED = wx.NewEventType()
wxEVT_AUI_PANE_DOCKING = wx.NewEventType()
wxEVT_AUI_PANE_DOCKED = wx.NewEventType()
wxEVT_AUI_PANE_ACTIVATED = wx.NewEventType()
wxEVT_AUI_PERSPECTIVE_CHANGED = wx.NewEventType()

EVT_AUI_PANE_BUTTON = wx.PyEventBinder(wxEVT_AUI_PANE_BUTTON, 0)
""" Fires an event when the user left-clicks on a pane button. """
EVT_AUI_PANE_CLOSE = wx.PyEventBinder(wxEVT_AUI_PANE_CLOSE, 0)
""" A pane in `AuiManager` has been closed. """
EVT_AUI_PANE_MAXIMIZE = wx.PyEventBinder(wxEVT_AUI_PANE_MAXIMIZE, 0)
""" A pane in `AuiManager` has been maximized. """
EVT_AUI_PANE_RESTORE = wx.PyEventBinder(wxEVT_AUI_PANE_RESTORE, 0)
""" A pane in `AuiManager` has been restored from a maximized state. """
EVT_AUI_RENDER = wx.PyEventBinder(wxEVT_AUI_RENDER, 0)
""" Fires an event every time the AUI frame is being repainted. """
EVT_AUI_FIND_MANAGER = wx.PyEventBinder(wxEVT_AUI_FIND_MANAGER, 0)
""" Used to find which AUI manager is controlling a certain pane. """
EVT_AUI_PANE_MINIMIZE = wx.PyEventBinder(wxEVT_AUI_PANE_MINIMIZE, 0)
""" A pane in `AuiManager` has been minimized. """
EVT_AUI_PANE_MIN_RESTORE = wx.PyEventBinder(wxEVT_AUI_PANE_MIN_RESTORE, 0)
""" A pane in `AuiManager` has been restored from a minimized state. """
EVT_AUI_PANE_FLOATING = wx.PyEventBinder(wxEVT_AUI_PANE_FLOATING, 0)
""" A pane in `AuiManager` is about to be floated. """
EVT_AUI_PANE_FLOATED = wx.PyEventBinder(wxEVT_AUI_PANE_FLOATED, 0)
""" A pane in `AuiManager` has been floated. """
EVT_AUI_PANE_DOCKING = wx.PyEventBinder(wxEVT_AUI_PANE_DOCKING, 0)
""" A pane in `AuiManager` is about to be docked. """
EVT_AUI_PANE_DOCKED = wx.PyEventBinder(wxEVT_AUI_PANE_DOCKED, 0)
""" A pane in `AuiManager` has been docked. """
EVT_AUI_PANE_ACTIVATED = wx.PyEventBinder(wxEVT_AUI_PANE_ACTIVATED, 0)
""" A pane in `AuiManager` has been activated. """
EVT_AUI_PERSPECTIVE_CHANGED = \
    wx.PyEventBinder(wxEVT_AUI_PERSPECTIVE_CHANGED, 0)
""" The layout in `AuiManager` has been changed. """


class CommandToolBarEvent(wx.PyCommandEvent):
    """ A specialized command event class for events sent by
    :class:`AuiToolBar`. """

    def __init__(self, command_type, win_id):
        """
        Default class constructor.

        :param `command_type`: the event kind or an instance of
        :class:`PyCommandEvent`.
        :param integer `win_id`: the window identification number.
        """

        if isinstance(command_type, int):
            wx.PyCommandEvent.__init__(self, command_type, win_id)
        else:
            wx.PyCommandEvent.__init__(self, command_type.GetEventType(),
                                       command_type.GetId())

        self.is_dropdown_clicked = False
        self.click_pt = wx.Point(-1, -1)
        self.rect = wx.Rect(-1, -1, 0, 0)
        self.tool_id = -1

    def IsDropDownClicked(self):
        """ Returns whether the drop down menu has been clicked. """

        return self.is_dropdown_clicked

    def SetDropDownClicked(self, c):
        """
        Sets whether the drop down menu has been clicked.

        :param bool `c`: ``True`` to set the drop down as clicked,
        ``False`` otherwise.
        """

        self.is_dropdown_clicked = c

    def GetClickPoint(self):
        """ Returns the point where the user clicked with the mouse. """

        return self.click_pt

    def SetClickPoint(self, p):
        """
        Sets the clicking point.

        :param Point `p`: the location of the mouse click.
        """

        self.click_pt = p

    def GetItemRect(self):
        """ Returns the :class:`AuiToolBarItem` rectangle. """

        return self.rect

    def SetItemRect(self, r):
        """
        Sets the :class:`AuiToolBarItem` rectangle.

        :param Rect `r`: the toolbar item rectangle.
        """

        self.rect = r

    def GetToolId(self):
        """ Returns the :class:`AuiToolBarItem` identifier. """

        return self.tool_id

    def SetToolId(self, id):
        """
        Sets the :class:`AuiToolBarItem` identifier.

        :param integer `id`: the toolbar item identifier.
        """

        self.tool_id = id


class AuiToolBarEvent(CommandToolBarEvent):
    """ A specialized command event class for events sent by
    :class:`AuiToolBar`. """

    def __init__(self, command_type=None, win_id=0):
        """
        Default class constructor.

        :param `command_type`: the event kind or an instance of
        :class:`PyCommandEvent`.
        :param integer `win_id`: the window identification number.
        """

        CommandToolBarEvent.__init__(self, command_type, win_id)

        if isinstance(command_type, int):
            self.notify = wx.NotifyEvent(command_type, win_id)
        else:
            self.notify = wx.NotifyEvent(command_type.GetEventType(),
                                         command_type.GetId())

    def GetNotifyEvent(self):
        """ Returns the actual :class:`NotifyEvent`. """

        return self.notify

    def IsAllowed(self):
        """ Returns whether the event is allowed or not. """

        return self.notify.IsAllowed()

    def Veto(self):
        """
        Prevents the change announced by this event from happening.

        It is in general a good idea to notify the user about the reasons for
        vetoing the change because otherwise the applications behaviour (which
        just refuses to do what the user wants) might be quite surprising.
        """

        self.notify.Veto()

    def Allow(self):
        """
        This is the opposite of :meth:`Veto`: it explicitly allows the event
        to be processed. For most events it is not necessary to call this
        method as the events are allowed anyhow but some are forbidden by
        default (this will be mentioned in the corresponding event
        description).
        """

        self.notify.Allow()


class CommandNotebookEvent(wx.PyCommandEvent):
    """ A specialized command event class for events sent by
    :class:`AuiNotebook` . """

    def __init__(self, command_type=None, win_id=0):
        """
        Default class constructor.

        :param `command_type`: the event kind or an instance of
        :class:`PyCommandEvent`.
        :param integer `win_id`: the window identification number.
        """

        if type(command_type) == int:
            wx.PyCommandEvent.__init__(self, command_type, win_id)
        else:
            wx.PyCommandEvent.__init__(self, command_type.GetEventType(),
                                       command_type.GetId())

        self.old_selection = -1
        self.selection = -1
        self.drag_source = None
        self.dispatched = 0
        self.label = ""
        self.editCancelled = False
        self.page = None

    def SetSelection(self, s):
        """
        Sets the selection member variable.

        :param integer `s`: the new selection.
        """

        self.selection = s
        self._commandInt = s

    def GetSelection(self):
        """ Returns the currently selected page, or -1 if
        none was selected. """

        return self.selection

    def SetOldSelection(self, s):
        """
        Sets the id of the page selected before the change.

        :param integer `s`: the old selection.
        """

        self.old_selection = s

    def GetOldSelection(self):
        """
        Returns the page that was selected before the change, or
        -1 if none was selected.
        """

        return self.old_selection

    def SetDragSource(self, s):
        """
        Sets the drag and drop source.

        :param `s`: the drag source.
        """

        self.drag_source = s

    def GetDragSource(self):
        """ Returns the drag and drop source. """

        return self.drag_source

    def SetDispatched(self, b):
        """
        Sets the event as dispatched
        (used for automatic :class:`AuiNotebook` ).

        :param `b`: whether the event was dispatched or not.
        """

        self.dispatched = b

    def GetDispatched(self):
        """ Returns whether the event was dispatched
        (used for automatic :class:`AuiNotebook` ). """

        return self.dispatched

    def IsEditCancelled(self):
        """ Returns the edit cancel flag
        (for ``EVT_AUINOTEBOOK_BEGIN`` | ``END_LABEL_EDIT`` only)."""

        return self.editCancelled

    def SetEditCanceled(self, editCancelled):
        """
        Sets the edit cancel flag
        (for ``EVT_AUINOTEBOOK_BEGIN`` | ``END_LABEL_EDIT`` only).

        :param bool `editCancelled`: whether the editing action has
        been cancelled or not.
        """

        self.editCancelled = editCancelled

    def GetLabel(self):
        """Returns the label-itemtext
        (for ``EVT_AUINOTEBOOK_BEGIN`` | ``END_LABEL_EDIT`` only)."""

        return self.label

    def SetLabel(self, label):
        """
        Sets the label. Useful only for ``EVT_AUINOTEBOOK_END_LABEL_EDIT``.

        :param string `label`: the new label.
        """

        self.label = label

    Page = property(lambda self: self.page,
                    lambda self, page: setattr(self, 'page', page))
    Selection = property(lambda self: self.GetSelection(),
                         lambda self, sel: self.SetSelection(sel))


class AuiNotebookEvent(CommandNotebookEvent):
    """ A specialized command event class for events sent by i
    :class:`AuiNotebook`. """

    def __init__(self, command_type=None, win_id=0):
        """
        Default class constructor.

        :param `command_type`: the event kind or an instance of
        :class:`PyCommandEvent`.
        :param integer `win_id`: the window identification number.
        """

        CommandNotebookEvent.__init__(self, command_type, win_id)

        if type(command_type) == int:
            self.notify = wx.NotifyEvent(command_type, win_id)
        else:
            self.notify = wx.NotifyEvent(command_type.GetEventType(),
                                         command_type.GetId())

    def GetNotifyEvent(self):
        """ Returns the actual :class:`NotifyEvent`. """

        return self.notify

    def IsAllowed(self):
        """ Returns whether the event is allowed or not. """

        return self.notify.IsAllowed()

    def Veto(self):
        """
        Prevents the change announced by this event from happening.

        It is in general a good idea to notify the user about the reasons for
        vetoing the change because otherwise the applications behaviour (which
        just refuses to do what the user wants) might be quite surprising.
        """

        self.notify.Veto()

    def Allow(self):
        """
        This is the opposite of :meth:`Veto`: it explicitly allows the event
        to be processed. For most events it is not necessary to call this
        method as the events are allowed anyhow but some are forbidden by
        default
        (this will be mentioned in the corresponding event description).
        """

        self.notify.Allow()


class AuiManagerEvent(wx.PyCommandEvent):
    """ A specialized command event class for events sent by
    :class:`AuiManager`. """

    def __init__(self, eventType, id=1):
        """
        Default class constructor.

        :param integer `eventType`: the event kind;
        :param integer `id`: the event identification number.
        """

        wx.PyCommandEvent.__init__(self, eventType, id)

        self.manager = None
        self.pane = None
        self.button = 0
        self.veto_flag = False
        self.canveto_flag = True
        self.dc = None

    def SetManager(self, mgr):
        """
        Associates a :class:`AuiManager` to the current event.

        :param `mgr`: an instance of :class:`AuiManager`.
        """

        self.manager = mgr

    def SetDC(self, pdc):
        """
        Associates a :class:`DC` device context to this event.

        :param `pdc`: a :class:`DC` device context object.
        """

        self.dc = pdc

    def SetPane(self, p):
        """
        Associates a :class:`AuiPaneInfo` instance to this event.

        :param `p`: a :class:`AuiPaneInfo` instance.
        """

        self.pane = p

    def SetButton(self, b):
        """
        Associates a :class:`AuiPaneButton` instance to this event.

        :param `b`: a :class:`AuiPaneButton` instance.
        """

        self.button = b

    def GetManager(self):
        """ Returns the associated :class:`AuiManager` (if any). """

        return self.manager

    def GetDC(self):
        """ Returns the associated :class:`DC` device context (if any). """

        return self.dc

    def GetPane(self):
        """ Returns the associated :class:`AuiPaneInfo` structure (if any). """

        return self.pane

    def GetButton(self):
        """ Returns the associated :class:`AuiPaneButton`
        instance (if any). """

        return self.button

    def Veto(self, veto=True):
        """
        Prevents the change announced by this event from happening.

        It is in general a good idea to notify the user about the reasons for
        vetoing the change because otherwise the applications behaviour (which
        just refuses to do what the user wants) might be quite surprising.

        :param bool `veto`: ``True`` to veto the event, ``False`` otherwise.
        """

        self.veto_flag = veto

    def GetVeto(self):
        """ Returns whether the event has been vetoed or not. """

        return self.veto_flag

    def SetCanVeto(self, can_veto):
        """
        Sets whether the event can be vetoed or not.

        :param bool `can_veto`: ``True`` if the event can be vetoed, i
        ``False`` otherwise.
        """

        self.canveto_flag = can_veto

    def CanVeto(self):
        """ Returns whether the event can be vetoed and has been vetoed. """

        return self.canveto_flag and self.veto_flag
