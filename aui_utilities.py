"""
This module contains some common functions used by :mod:`lib.agw.aui` to
manipulate colours, bitmaps, text, gradient shadings and custom dragging images
for :class:`~lib.agw.aui.auibook.AuiNotebook` tabs.
"""

__author__ = "Andrea Gavana <andrea.gavana@gmail.com>"
__date__ = "31 March 2009"


import wx

from . import aui_constants, aui_events


if wx.Platform == "__WXMAC__":
    import Carbon.Appearance


def BlendColour(fg, bg, alpha):
    """
    Blends the two colour component `fg` and `bg` into one colour component,
    adding an optional alpha channel.

    :param Colour `fg`: the first colour component;
    :param Colour `bg`: the second colour component;
    :param integer `alpha`: an optional transparency value.
    """

    result = bg + (alpha*(fg - bg))

    if result < 0.0:
        result = 0.0
    if result > 255:
        result = 255

    return result


def StepColour(c, ialpha):
    """
    Darken/lighten the input colour `c`.

    :param Colour `c`: a colour to darken/lighten;
    :param integer `ialpha`: a transparency value.
    """

    if ialpha == 100:
        return c

    r, g, b = c.Red(), c.Green(), c.Blue()

    # ialpha is 0..200 where 0 is completely black
    # and 200 is completely white and 100 is the same
    # convert that to normal alpha 0.0 - 1.0
    ialpha = min(ialpha, 200)
    ialpha = max(ialpha, 0)
    alpha = (ialpha - 100.0)/100.0

    if ialpha > 100:

        # blend with white
        bg = 255
        alpha = 1.0 - alpha  # 0 = transparent fg 1 = opaque fg

    else:

        # blend with black
        bg = 0
        alpha = 1.0 + alpha  # 0 = transparent fg 1 = opaque fg

    r = BlendColour(r, bg, alpha)
    g = BlendColour(g, bg, alpha)
    b = BlendColour(b, bg, alpha)

    return wx.Colour(r, g, b)


def LightContrastColour(c):
    """
    Creates a new, lighter colour based on the input colour `c`.

    :param Colour `c`: the input colour to analyze.
    """

    amount = 120

    # if the colour is especially dark, then
    # make the contrast even lighter
    if c.Red() < 128 and c.Green() < 128 and c.Blue() < 128:
        amount = 160

    return StepColour(c, amount)


def ChopText(dc, text, max_size):
    """
    Chops the input `text` if its size does not fit in `max_size`, by
    cutting the text and adding ellipsis at the end.

    :param `dc`: a :class:`DC` device context;
    :param string `text`: the text to chop;
    :param integer `max_size`: the maximum size in which the text should fit.
    """

    # first check if the text fits with no problems
    x, y, dummy = dc.GetFullMultiLineTextExtent(text)

    if x <= max_size:
        return text

    textLen = len(text)
    last_good_length = 0

    for i in range(textLen, -1, -1):
        s = text[0:i]
        s += "..."

        x, y = dc.GetTextExtent(s)
        last_good_length = i

        if x < max_size:
            break

    ret = text[0:last_good_length] + "..."
    return ret


def BitmapFromBits(bits, w, h, colour):
    """
    A utility function that creates a masked bitmap from raw bits (XBM format).

    :param string `bits`: the raw bits of the bitmap;
    :param integer `w`: the bitmap width;
    :param integer `h`: the bitmap height;
    :param Colour `colour`: the colour which will replace all white pixels
     in the raw bitmap.
    """

    img = wx.Bitmap(bits, w, h).ConvertToImage()
    img.Replace(0, 0, 0, 123, 123, 123)
    img.Replace(255, 255, 255, colour.Red(), colour.Green(), colour.Blue())
    img.SetMaskColour(123, 123, 123)
    return wx.Bitmap(img)


def IndentPressedBitmap(rect, button_state):
    """
    Indents the input rectangle `rect` based on the value of `button_state`.

    :param Rect `rect`: the button bitmap rectangle;
    :param integer `button_state`: the button state.
    """

    if button_state == aui_constants.AUI_BUTTON_STATE_PRESSED:
        rect.x += 1
        rect.y += 1

    return rect


def GetBaseColour():
    """
    Returns the face shading colour on push buttons/backgrounds,
    mimicking as closely as possible the platform UI colours.
    """

    if wx.Platform == "__WXMAC__":

        if hasattr(wx, 'MacThemeColour'):
            base_colour = wx.MacThemeColour(Carbon.Appearance.
                                            kThemeBrushToolbarBackground)
        else:
            brush = wx.Brush(wx.BLACK)
            brush.MacSetTheme(Carbon.Appearance.kThemeBrushToolbarBackground)
            base_colour = brush.GetColour()

    else:

        base_colour = wx.SystemSettings.GetColour(wx.SYS_COLOUR_3DFACE)

    # the base_colour is too pale to use as our base colour,
    # so darken it a bit
    if ((255-base_colour.Red()) + (255-base_colour.Green()) +
            (255-base_colour.Blue()) < 60):

        base_colour = StepColour(base_colour, 92)

    return base_colour


def MakeDisabledBitmap(bitmap):
    """
    Convert the given image (in place) to a grayed-out version, appropriate
    for a 'disabled' appearance.

    :param Bitmap `bitmap`: the bitmap to gray-out.
    """

    if wx.VERSION_STRING >= '2.9.0':
        return bitmap.ConvertToDisabled()

    anImage = bitmap.ConvertToImage()
    factor = 0.7        # 0 < f < 1.  Higher Is Grayer

    if anImage.HasMask():
        maskColour = (anImage.GetMaskRed(), anImage.GetMaskGreen(),
                      anImage.GetMaskBlue())
    else:
        maskColour = None

    data = list(map(ord, list(anImage.GetData())))

    for i in range(0, len(data), 3):

        pixel = (data[i], data[i+1], data[i+2])
        pixel = MakeGray(pixel, factor, maskColour)

        for x in range(3):
            data[i+x] = pixel[x]

    anImage.SetData(''.join(map(chr, data)))

    return anImage.ConvertToBitmap()


def MakeGray(rgbTuple, factor, maskColour):
    """
    Make a pixel grayed-out.

    If the pixel matches the `maskColour`, it won't be changed.

    :param tuple `rgbTuple`: a tuple representing a pixel colour;
    :param integer `factor`: a graying-out factor;
    :param Colour `maskColour`: a colour mask.
    """

    if rgbTuple != maskColour:
        r, g, b = rgbTuple
        return [int((230 - x) * factor) + x for x in (r, g, b)]
    else:
        return rgbTuple


def Clip(a, b, c):
    """
    Clips the value in `a` based on the extremes `b` and `c`.

    :param `a`: the value to analyze;
    :param `b`: a minimum value;
    :param `c`: a maximum value.
    """

    return ((a < b and [b]) or [(a > c and [c] or [a])[0]])[0]


def LightColour(colour, percent):
    """
    Brighten input `colour` by `percent`.

    :param Colour `colour`: the colour to be brightened;
    :param integer `percent`: brightening percentage.
    """

    end_colour = wx.WHITE

    rd = end_colour.Red() - colour.Red()
    gd = end_colour.Green() - colour.Green()
    bd = end_colour.Blue() - colour.Blue()

    high = 100

    # We take the percent way of the colour from colour -. white
    i = percent
    r = colour.Red() + ((i*rd*100)/high)/100
    g = colour.Green() + ((i*gd*100)/high)/100
    b = colour.Blue() + ((i*bd*100)/high)/100
    return wx.Colour(r, g, b)


def PaneCreateStippleBitmap():
    """
    Creates a stipple bitmap to be used in a :class:`Brush`.

    This is used to draw sash resize hints.
    """

    data = [0, 0, 0, 192, 192, 192, 192, 192, 192, 0, 0, 0]
    img = wx.EmptyImage(2, 2)
    counter = 0

    for ii in range(2):
        for jj in range(2):
            img.SetRGB(ii, jj, data[counter], data[counter+1], data[counter+2])
            counter = counter + 3

    return img.ConvertToBitmap()


def DrawMACCloseButton(colour, backColour=None):
    """
    Draws the wxMAC tab close button using :class:`GraphicsContext`.

    :param Colour `colour`: the colour to use to draw the circle;
    :param Colour `backColour`: the optional background colour for the circle.
    """

    bmp = wx.EmptyBitmapRGBA(16, 16)
    dc = wx.MemoryDC()
    dc.SelectObject(bmp)

    gc = wx.GraphicsContext.Create(dc)
    gc.SetBrush(wx.Brush(colour))
    path = gc.CreatePath()
    path.AddCircle(6.5, 7, 6.5)
    path.CloseSubpath()
    gc.FillPath(path)

    path = gc.CreatePath()
    if backColour is not None:
        pen = wx.Pen(backColour, 2)
    else:
        pen = wx.Pen("white", 2)

    pen.SetCap(wx.CAP_BUTT)
    pen.SetJoin(wx.JOIN_BEVEL)
    gc.SetPen(pen)
    path.MoveToPoint(3.5, 4)
    path.AddLineToPoint(9.5, 10)
    path.MoveToPoint(3.5, 10)
    path.AddLineToPoint(9.5, 4)
    path.CloseSubpath()
    gc.DrawPath(path)

    dc.SelectObject(wx.NullBitmap)
    return bmp


def DarkenBitmap(bmp, caption_colour, new_colour):
    """
    Darkens the input bitmap on wxMAC using the input colour.

    :param Bitmap `bmp`: the bitmap to be manipulated;
    :param Colour `caption_colour`: the colour of the pane caption;
    :param Colour `new_colour`: the colour used to darken the bitmap.
    """

    image = bmp.ConvertToImage()
    red = caption_colour.Red()/float(new_colour.Red())
    green = caption_colour.Green()/float(new_colour.Green())
    blue = caption_colour.Blue()/float(new_colour.Blue())
    image = image.AdjustChannels(red, green, blue)
    return image.ConvertToBitmap()


def DrawGradientRectangle(dc, rect, start_colour, end_colour, direction,
                          offset=0, length=0):
    """
    Draws a gradient-shaded rectangle.

    :param `dc`: a :class:`DC` device context;
    :param Rect `rect`: the rectangle in which to draw the gradient;
    :param Colour `start_colour`: the first colour of the gradient;
    :param Colour `end_colour`: the second colour of the gradient;
    :param integer `direction`: the gradient direction
    (horizontal or vertical).
    """

    if direction == aui_constants.AUI_GRADIENT_VERTICAL:
        dc.GradientFillLinear(rect, start_colour, end_colour, wx.SOUTH)
    else:
        dc.GradientFillLinear(rect, start_colour, end_colour, wx.EAST)


def FindFocusDescendant(ancestor):
    """
    Find a window with the focus, that is also a descendant of the given
    window. This is used to determine the window to initially send commands to.

    :param Window `ancestor`: the window to check for ancestry.
    """

    # Process events starting with the window with the focus, if any.
    focusWin = wx.Window.FindFocus()
    win = focusWin

    # Check if this is a descendant of this frame.
    # If not, win will be set to NULL.
    while win:
        if win == ancestor:
            break
        else:
            win = win.GetParent()

    if win is None:
        focusWin = None

    return focusWin


def GetLabelSize(dc, label, vertical):
    """
    Returns the :class:`~lib.agw.aui.auibar.AuiToolBar` item label size.

    :param string `label`: the toolbar tool label;
    :param bool `vertical`: whether the toolbar tool orientation is vertical
     or not.
    """

    text_width = text_height = 0

    # get the text height
    dummy, text_height = dc.GetTextExtent("ABCDHgj")
    # get the text width
    if label.strip():
        text_width, dummy = dc.GetTextExtent(label)

    if vertical:
        tmp = text_height
        text_height = text_width
        text_width = tmp

    return wx.Size(text_width, text_height)


#---------------------------------------------------------------------------
# TabDragImage implementation
# This class handles the creation of a custom image when dragging
# AuiNotebook tabs
#---------------------------------------------------------------------------

class TabDragImage(wx.DragImage):
    """
    This class handles the creation of a custom image in case of drag and
    drop of a notebook tab.
    """

    def __init__(self, notebook, page, button_state, tabArt):
        """
        Default class constructor.

        For internal use: do not call it in your code!

        :param `notebook`: an instance of
               :class:`~lib.agw.aui.auibook.AuiNotebook`;
        :param `page`: the dragged
               :class:`~lib.agw.aui.auibook.AuiNotebookPage` page;
        :param integer `button_state`: the state of the close button on the
               tab;
        :param `tabArt`: an instance of
               :class:`~lib.agw.aui.tabart.AuiDefaultTabArt` or one of its
               derivations.
        """

        self._backgroundColour = wx.NamedColour("pink")
        self._bitmap = self.CreateBitmap(notebook, page, button_state, tabArt)
        wx.DragImage.__init__(self, self._bitmap)

    def CreateBitmap(self, notebook, page, button_state, tabArt):
        """
        Actually creates the drag and drop bitmap.

        :param `notebook`: an instance of
            :class:`~lib.agw.aui.auibook.AuiNotebook`;
        :param `page`: the dragged
            :class:`~lib.agw.aui.auibook.AuiNotebookPage` page;
        :param integer `button_state`: the state of the close button on the
            tab;
        :param `tabArt`: an instance of
            :class:`~lib.agw.aui.tabart.AuiDefaultTabArt`
            or one of its derivations.
        """

        control = page.control
        memory = wx.MemoryDC(wx.EmptyBitmap(1, 1))

        tab_size, x_extent = \
            tabArt.GetTabSize(memory, notebook, page.caption, page.bitmap,
                              page.active, button_state, control)

        tab_width, tab_height = tab_size
        rect = wx.Rect(0, 0, tab_width, tab_height)

        bitmap = wx.EmptyBitmap(tab_width+1, tab_height+1)
        memory.SelectObject(bitmap)

        if wx.Platform == "__WXMAC__":
            memory.SetBackground(wx.TRANSPARENT_BRUSH)
        else:
            memory.SetBackground(wx.Brush(self._backgroundColour))

        memory.SetBackgroundMode(wx.TRANSPARENT)
        memory.Clear()

        paint_control = wx.Platform != "__WXMAC__"
        tabArt.DrawTab(memory, notebook, page, rect, button_state,
                       paint_control=paint_control)

        memory.SetBrush(wx.TRANSPARENT_BRUSH)
        memory.SetPen(wx.BLACK_PEN)
        memory.DrawRoundedRectangle(0, 0, tab_width+1, tab_height+1, 2)

        memory.SelectObject(wx.NullBitmap)

        # Gtk and Windows unfortunatly don't do so well with transparent
        # drawing so this hack corrects the image to have a transparent
        # background.
        if wx.Platform != '__WXMAC__':
            timg = bitmap.ConvertToImage()
            if not timg.HasAlpha():
                timg.InitAlpha()
            for y in range(timg.GetHeight()):
                for x in range(timg.GetWidth()):
                    pix = wx.Colour(timg.GetRed(x, y),
                                    timg.GetGreen(x, y),
                                    timg.GetBlue(x, y))
                    if pix == self._backgroundColour:
                        timg.SetAlpha(x, y, 0)
            bitmap = timg.ConvertToBitmap()
        return bitmap


def GetDockingImage(direction, useAero, center):
    """
    Returns the correct name of the docking bitmap depending on the input
    parameters.

    :param bool `useAero`: whether
    :class:`~lib.agw.aui.framemanager.AuiManager` is using
     Aero-style or Whidbey-style docking images or not;
    :param bool `center`: whether we are looking for the center
    diamond-shaped bitmap or not.
    """

    suffix = (center and [""] or ["_single"])[0]
    prefix = "aui_constants."
    if useAero == 2:
        # Whidbey docking guides
        prefix = "aui_constants.whidbey_"
    elif useAero == 1:
        # Aero docking style
        prefix = "aui_constants.aero_"

    if direction == wx.TOP:
        bmp_unfocus = eval("%sup%s" % (prefix, suffix)).GetBitmap()
        bmp_focus = eval("%sup_focus%s" % (prefix, suffix)).GetBitmap()
    elif direction == wx.BOTTOM:
        bmp_unfocus = eval("%sdown%s" % (prefix, suffix)).GetBitmap()
        bmp_focus = eval("%sdown_focus%s" % (prefix, suffix)).GetBitmap()
    elif direction == wx.LEFT:
        bmp_unfocus = eval("%sleft%s" % (prefix, suffix)).GetBitmap()
        bmp_focus = eval("%sleft_focus%s" % (prefix, suffix)).GetBitmap()
    elif direction == wx.RIGHT:
        bmp_unfocus = eval("%sright%s" % (prefix, suffix)).GetBitmap()
        bmp_focus = eval("%sright_focus%s" % (prefix, suffix)).GetBitmap()
    else:
        bmp_unfocus = eval("%stab%s" % (prefix, suffix)).GetBitmap()
        bmp_focus = eval("%stab_focus%s" % (prefix, suffix)).GetBitmap()

    return bmp_unfocus, bmp_focus


def TakeScreenShot(rect):
    """
    Takes a screenshot of the screen at given position and size (`rect`).

    :param Rect `rect`: the screen rectangle for which we want to take a
    screenshot.
    """

    # Create a DC for the whole screen area
    dcScreen = wx.ScreenDC()

    # Create a Bitmap that will later on hold the screenshot image
    # Note that the Bitmap must have a size big enough to hold the screenshot
    # -1 means using the current default colour depth
    bmp = wx.EmptyBitmap(rect.width, rect.height)

    # Create a memory DC that will be used for actually taking the screenshot
    memDC = wx.MemoryDC()

    # Tell the memory DC to use our Bitmap
    # all drawing action on the memory DC will go to the Bitmap now
    memDC.SelectObject(bmp)

    # Blit (in this case copy) the actual screen on the memory DC
    # and thus the Bitmap
    memDC.Blit(0,            # Copy to this X coordinate
               0,            # Copy to this Y coordinate
               rect.width,   # Copy this width
               rect.height,  # Copy this height
               dcScreen,     # From where do we copy?
               rect.x,       # What's the X offset in the original DC?
               rect.y        # What's the Y offset in the original DC?
               )

    # Select the Bitmap out of the memory DC by selecting a new
    # uninitialized Bitmap
    memDC.SelectObject(wx.NullBitmap)

    return bmp


def RescaleScreenShot(bmp, thumbnail_size=200):
    """
    Rescales a bitmap to be `thumbnail_size` pixels wide (or tall) at maximum.

    :param Bitmap `bmp`: the bitmap to rescale;
    :param integer `thumbnail_size`: the maximum size of every page thumbnail.
    """

    bmpW, bmpH = bmp.GetWidth(), bmp.GetHeight()
    img = bmp.ConvertToImage()

    newW, newH = bmpW, bmpH

    if bmpW > bmpH:
        if bmpW > thumbnail_size:
            ratio = bmpW/float(thumbnail_size)
            newW, newH = int(bmpW/ratio), int(bmpH/ratio)
            img.Rescale(newW, newH, wx.IMAGE_QUALITY_HIGH)
    else:
        if bmpH > thumbnail_size:
            ratio = bmpH/float(thumbnail_size)
            newW, newH = int(bmpW/ratio), int(bmpH/ratio)
            img.Rescale(newW, newH, wx.IMAGE_QUALITY_HIGH)

    newBmp = img.ConvertToBitmap()
    otherBmp = wx.EmptyBitmap(newW+5, newH+5)

    memDC = wx.MemoryDC()
    memDC.SelectObject(otherBmp)
    memDC.SetBackground(wx.WHITE_BRUSH)
    memDC.Clear()

    memDC.SetPen(wx.TRANSPARENT_PEN)

    pos = 0
    for i in range(5, 0, -1):
        brush = wx.Brush(wx.Colour(50*i, 50*i, 50*i))
        memDC.SetBrush(brush)
        memDC.DrawRoundedRectangle(0, 0, newW+5-pos, newH+5-pos, 2)
        pos += 1

    memDC.DrawBitmap(newBmp, 0, 0, True)

    # Select the Bitmap out of the memory DC by selecting a new
    # uninitialized Bitmap
    memDC.SelectObject(wx.NullBitmap)

    return otherBmp


def GetSlidingPoints(rect, size, direction):
    """
    Returns the point at which the sliding in and out of a minimized pane
    begins.

    :param Rect `rect`: the :class:`~lib.agw.aui.auibar.AuiToolBar` tool
    screen rectangle;
    :param Size `size`: the pane window size;
    :param integer `direction`: the pane docking direction.
    """

    if direction == aui_constants.AUI_DOCK_LEFT:
        startX, startY = rect.x + rect.width + 2, rect.y
    elif direction == aui_constants.AUI_DOCK_TOP:
        startX, startY = rect.x, rect.y + rect.height + 2
    elif direction == aui_constants.AUI_DOCK_RIGHT:
        startX, startY = rect.x - size.x - 2, rect.y
    elif direction == aui_constants.AUI_DOCK_BOTTOM:
        startX, startY = rect.x, rect.y - size.y - 2
    else:
        raise Exception("How did we get here?")

    caption_height = wx.SystemSettings.GetMetric(wx.SYS_CAPTION_Y)
    frame_border_x = wx.SystemSettings.GetMetric(wx.SYS_FRAMESIZE_X)
    frame_border_y = wx.SystemSettings.GetMetric(wx.SYS_FRAMESIZE_Y)

    stopX = size.x + caption_height + frame_border_x
    stopY = size.x + frame_border_y

    return startX, startY, stopX, stopY


def CopyAttributes(newArt, oldArt):
    """
    Copies pens, brushes, colours and fonts from the old tab art to the new
    one.

    :param `newArt`: the new instance of
    :class:`~lib.agw.aui.tabart.AuiDefaultTabArt`;
    :param `oldArt`: the old instance of
    :class:`~lib.agw.aui.tabart.AuiDefaultTabArt`.
    """

    attrs = dir(oldArt)

    for attr in attrs:
        if attr.startswith("_") and \
                (attr.endswith("_colour") or attr.endswith("_font") or
                 attr.endswith("_font") or attr.endswith("_brush") or
                 attr.endswith("Pen") or attr.endswith("_pen")):
            setattr(newArt, attr, getattr(oldArt, attr))

    return newArt


def GetManager(window):
    """
    This function will return the aui manager for a given window.

    :param Window `window`: this parameter should be any child window or
     grand-child window (and so on) of the frame/window managed by
     :class:`AuiManager`. The window does not need to be managed by the
     manager itself, nor does it even need
     to be a child or sub-child of a managed window. It must however be inside
     the window hierarchy underneath the managed window.
    """

    #TODO: This does not work the exact same as before. Figure out if the
    #Differences ever matter
    if hasattr(window, 'GetAuiManager') and not \
            isinstance(wx.GetTopLevelParent(window), wx.MiniFrame):
        return window.GetAuiManager()

    evt = aui_events.AuiManagerEvent(aui_events.wxEVT_AUI_FIND_MANAGER)
    evt.SetManager(None)
    evt.ResumePropagation(wx.EVENT_PROPAGATE_MAX)

    if not window.GetEventHandler().ProcessEvent(evt):
        return None

    return evt.GetManager()


class AuiPaneInfo(object):
    """
    AuiPaneInfo specifies all the parameters for a pane. These parameters
    specify where the pane is on the screen, whether it is docked or floating,
    or hidden. In addition, these parameters specify the pane's docked
    position, floating position, preferred
    size, minimum size, caption text among many other parameters.
    """

    optionFloating = 2**0
    optionHidden = 2**1
    optionLeftDockable = 2**2
    optionRightDockable = 2**3
    optionTopDockable = 2**4
    optionBottomDockable = 2**5
    optionFloatable = 2**6
    optionMovable = 2**7
    optionResizable = 2**8
    optionPaneBorder = 2**9
    optionCaption = 2**10
    optionGripper = 2**11
    optionDestroyOnClose = 2**12
    optionToolbar = 2**13
    optionActive = 2**14
    optionGripperTop = 2**15
    optionMaximized = 2**16
    optionDockFixed = 2**17
    optionNotebookDockable = 2**18
    optionMinimized = 2**19
    optionLeftSnapped = 2**20
    optionRightSnapped = 2**21
    optionTopSnapped = 2**22
    optionBottomSnapped = 2**23
    optionFlyOut = 2**24
    optionCaptionLeft = 2**25

    buttonClose = 2**26
    buttonMaximize = 2**27
    buttonMinimize = 2**28
    buttonPin = 2**29

    buttonCustom1 = 2**30
    buttonCustom2 = 2**31
    buttonCustom3 = 2**32

    savedHiddenState = 2**33    # used internally
    actionPane = 2**34    # used internally
    wasMaximized = 2**35    # used internally
    needsRestore = 2**36    # used internally

    def __init__(self):
        """ Default class constructor. """

        self.window = None
        self.frame = None
        self.state = 0
        self.dock_direction = aui_constants.AUI_DOCK_LEFT
        self.dock_layer = 0
        self.dock_row = 0
        self.dock_pos = 0
        self.minimize_mode = aui_constants.AUI_MINIMIZE_POS_SMART
        self.floating_pos = wx.Point(-1, -1)
        self.floating_size = wx.Size(-1, -1)
        self.best_size = wx.Size(-1, -1)
        self.min_size = wx.Size(-1, -1)
        self.max_size = wx.Size(-1, -1)
        self.dock_proportion = 0
        self.caption = ""
        self.buttons = []
        self.name = ""
        self.icon = wx.NullIcon
        self.rect = wx.Rect()
        self.notebook_id = -1
        self.transparent = 255
        self.needsTransparency = False
        self.previousDockPos = None
        self.previousDockSize = 0
        self.snapped = 0
        self.minimize_target = None

        self.DefaultPane()

    def dock_direction_get(self):
        """
        Getter for the `dock_direction`.

        :see: :meth:`~AuiPaneInfo.dock_direction_set`
        for a set of valid docking directions.
        """

        if self.IsMaximized():
            return aui_constants.AUI_DOCK_CENTER
        else:
            return self._dock_direction

    def dock_direction_set(self, value):
        """
        Setter for the `dock_direction`.

        :param integer `value`: the docking direction.
         This can be one of the following bits:

        ========================== ===== ======================================
        Dock Flag                  Value  Description
        ========================== ===== ======================================
        ``AUI_DOCK_NONE``              0 No docking direction.
        ``AUI_DOCK_TOP``               1 Top docking direction.
        ``AUI_DOCK_RIGHT``             2 Right docking direction.
        ``AUI_DOCK_BOTTOM``            3 Bottom docking direction.
        ``AUI_DOCK_LEFT``              4 Left docking direction.
        ``AUI_DOCK_CENTER``            5 Center docking direction.
        ``AUI_DOCK_CENTRE``            5 Centre docking direction.
        ``AUI_DOCK_NOTEBOOK_PAGE``     6 Automatic AuiNotebooks docking style.
        ========================== ===== ======================================

        """

        self._dock_direction = value

    dock_direction = property(dock_direction_get, dock_direction_set)

    def IsOk(self):
        """
        Returns ``True`` if the :class:`AuiPaneInfo` structure is valid.

        :note: A pane structure is valid if it has an associated window.
        """

        return self.window is not None

    def IsMaximized(self):
        """ Returns ``True`` if the pane is maximized. """

        return self.HasFlag(self.optionMaximized)

    def IsMinimized(self):
        """ Returns ``True`` if the pane is minimized. """

        return self.HasFlag(self.optionMinimized)

    def IsFixed(self):
        """ Returns ``True`` if the pane cannot be resized. """

        return not self.HasFlag(self.optionResizable)

    def IsResizeable(self):
        """ Returns ``True`` if the pane can be resized. """

        return self.HasFlag(self.optionResizable)

    def IsShown(self):
        """ Returns ``True`` if the pane is currently shown. """

        return not self.HasFlag(self.optionHidden)

    def IsFloating(self):
        """ Returns ``True`` if the pane is floating. """

        return self.HasFlag(self.optionFloating)

    def IsDocked(self):
        """ Returns ``True`` if the pane is docked. """

        return not self.HasFlag(self.optionFloating)

    def IsToolbar(self):
        """ Returns ``True`` if the pane contains a toolbar. """

        return self.HasFlag(self.optionToolbar)

    def IsTopDockable(self):
        """
        Returns ``True`` if the pane can be docked at the top
        of the managed frame.
        """

        return self.HasFlag(self.optionTopDockable)

    def IsBottomDockable(self):
        """
        Returns ``True`` if the pane can be docked at the bottom
        of the managed frame.
        """

        return self.HasFlag(self.optionBottomDockable)

    def IsLeftDockable(self):
        """
        Returns ``True`` if the pane can be docked at the left
        of the managed frame.
        """

        return self.HasFlag(self.optionLeftDockable)

    def IsRightDockable(self):
        """
        Returns ``True`` if the pane can be docked at the right
        of the managed frame.
        """

        return self.HasFlag(self.optionRightDockable)

    def IsDockable(self):
        """ Returns ``True`` if the pane can be docked. """

        return self.IsTopDockable() or self.IsBottomDockable() or \
            self.IsLeftDockable() or self.IsRightDockable() or \
            self.IsNotebookDockable()

    def IsFloatable(self):
        """
        Returns ``True`` if the pane can be undocked and displayed as a
        floating window.
        """

        return self.HasFlag(self.optionFloatable)

    def IsMovable(self):
        """
        Returns ``True`` if the docked frame can be undocked or moved to
        another dock position.
        """

        return self.HasFlag(self.optionMovable)

    def IsDestroyOnClose(self):
        """
        Returns ``True`` if the pane should be destroyed when it is closed.

        Normally a pane is simply hidden when the close button is clicked.
        Calling :meth:`~AuiPaneInfo.DestroyOnClose` with a ``True`` input
        parameter will cause the window to be destroyed when the user clicks
        the pane's close button.
        """

        return self.HasFlag(self.optionDestroyOnClose)

    def IsNotebookDockable(self):
        """
        Returns ``True`` if a pane can be docked on top to another to create a
        :class:`~lib.agw.aui.auibook.AuiNotebook`.
        """

        return self.HasFlag(self.optionNotebookDockable)

    def IsTopSnappable(self):
        """ Returns ``True`` if the pane can be snapped at the top of the
        managed frame. """

        return self.HasFlag(self.optionTopSnapped)

    def IsBottomSnappable(self):
        """ Returns ``True`` if the pane can be snapped at the bottom of the
        managed frame. """

        return self.HasFlag(self.optionBottomSnapped)

    def IsLeftSnappable(self):
        """ Returns ``True`` if the pane can be snapped on the left of the
        managed frame. """

        return self.HasFlag(self.optionLeftSnapped)

    def IsRightSnappable(self):
        """ Returns ``True`` if the pane can be snapped on the right of the
        managed frame. """

        return self.HasFlag(self.optionRightSnapped)

    def IsSnappable(self):
        """ Returns ``True`` if the pane can be snapped. """

        return self.IsTopSnappable() or self.IsBottomSnappable() or \
            self.IsLeftSnappable() or self.IsRightSnappable()

    def IsFlyOut(self):
        """ Returns ``True`` if the floating pane has a "fly-out" effect. """

        return self.HasFlag(self.optionFlyOut)

    def HasCaption(self):
        """ Returns ``True`` if the pane displays a caption. """

        return self.HasFlag(self.optionCaption)

    def HasCaptionLeft(self):
        """ Returns ``True`` if the pane displays a caption on the left
        (rotated by 90 degrees). """

        return self.HasFlag(self.optionCaptionLeft)

    def HasGripper(self):
        """ Returns ``True`` if the pane displays a gripper. """

        return self.HasFlag(self.optionGripper)

    def HasBorder(self):
        """ Returns ``True`` if the pane displays a border. """

        return self.HasFlag(self.optionPaneBorder)

    def HasCloseButton(self):
        """ Returns ``True`` if the pane displays a button to close the
        pane. """

        return self.HasFlag(self.buttonClose)

    def HasMaximizeButton(self):
        """ Returns ``True`` if the pane displays a button to maximize the
        pane. """

        return self.HasFlag(self.buttonMaximize)

    def HasMinimizeButton(self):
        """ Returns ``True`` if the pane displays a button to minimize the
        pane. """

        return self.HasFlag(self.buttonMinimize)

    def GetMinimizeMode(self):
        """
        Returns the minimization style for this pane.

        Possible return values are:

        ============================== ========= ==============================
        Minimize Mode Flag             Hex Value Description
        ============================== ========= ==============================
        ``AUI_MINIMIZE_POS_SMART``          0x01 Minimizes the pane on the
                                                 closest tool bar
        ``AUI_MINIMIZE_POS_TOP``            0x02 Minimizes the pane on the top
                                                 tool bar
        ``AUI_MINIMIZE_POS_LEFT``           0x03 Minimizes the pane on its left
                                                 tool bar
        ``AUI_MINIMIZE_POS_RIGHT``          0x04 Minimizes the pane on its
                                                 right tool bar
        ``AUI_MINIMIZE_POS_BOTTOM``         0x05 Minimizes the pane on its
                                                 bottom tool bar
        ``AUI_MINIMIZE_POS_TOOLBAR``        0x06 Minimizes the pane on a target
                                                 :class:`~lib.agw.aui.auibar.
                                                 AuiToolBar`
        ``AUI_MINIMIZE_POS_MASK``           0x17 Mask to filter the position
                                                 flags
        ``AUI_MINIMIZE_CAPT_HIDE``           0x0 Hides the caption of the
                                                 minimized pane
        ``AUI_MINIMIZE_CAPT_SMART``         0x08 Displays the caption in the
                                                 best rotation (horizontal or
                                                 clockwise)
        ``AUI_MINIMIZE_CAPT_HORZ``          0x10 Displays the caption
                                                 horizontally
        ``AUI_MINIMIZE_CAPT_MASK``          0x18 Mask to filter the caption
                                                 flags
        ============================== ========= ==============================

        The flags can be filtered with the following masks:

        ============================== ========= ==============================
        Minimize Mask Flag             Hex Value Description
        ============================== ========= ==============================
        ``AUI_MINIMIZE_POS_MASK``           0x17 Filters the position flags
        ``AUI_MINIMIZE_CAPT_MASK``          0x18 Filters the caption flags
        ============================== ========= ==============================

        """

        return self.minimize_mode

    def HasPinButton(self):
        """ Returns ``True`` if the pane displays a button
        to float the pane. """

        return self.HasFlag(self.buttonPin)

    def HasGripperTop(self):
        """ Returns ``True`` if the pane displays a gripper at the top. """

        return self.HasFlag(self.optionGripperTop)

    def Window(self, w):
        """
        Associate a :class:`Window` derived window to this pane.

        This normally does not need to be specified, as the window pointer is
        automatically assigned to the :class:`AuiPaneInfo`
        structure as soon as it is added to the manager.

        :param `w`: a :class:`Window` derived window.
        """

        self.window = w
        return self

    def Name(self, name):
        """
        Sets the name of the pane so it can be referenced in lookup functions.

        If a name is not specified by the user, a random name is assigned
        to the pane when it is added to the manager.

        :param `name`: a string specifying the pane name.

        .. warning::

           If you are using :meth:`AuiManager.SavePerspective` and
           :meth:`AuiManager.LoadPerspective`, you will have to specify a name
           for your pane using :meth:`~AuiPaneInfo.Name`, as perspectives
           containing randomly generated names can not be properly restored.
        """

        self.name = name
        return self

    def Caption(self, caption):
        """
        Sets the caption of the pane.

        :param string `caption`: a string specifying the pane caption.
        """

        self.caption = caption
        return self

    def Left(self):
        """
        Sets the pane dock position to the left side of the frame.

        :note: This is the same thing as calling
        :meth:`~AuiPaneInfo.Direction` with ``AUI_DOCK_LEFT`` as parameter.
        """

        self.dock_direction = aui_constants.AUI_DOCK_LEFT
        return self

    def Right(self):
        """
        Sets the pane dock position to the right side of the frame.

        :note: This is the same thing as calling
        :meth:`~AuiPaneInfo.Direction` with ``AUI_DOCK_RIGHT`` as parameter.
        """

        self.dock_direction = aui_constants.AUI_DOCK_RIGHT
        return self

    def Top(self):
        """
        Sets the pane dock position to the top of the frame.

        :note: This is the same thing as calling
        :meth:`~AuiPaneInfo.Direction` with ``AUI_DOCK_TOP`` as parameter.
        """

        self.dock_direction = aui_constants.AUI_DOCK_TOP
        return self

    def Bottom(self):
        """
        Sets the pane dock position to the bottom of the frame.

        :note: This is the same thing as calling
        :meth:`~AuiPaneInfo.Direction` with ``AUI_DOCK_BOTTOM`` as parameter.
        """

        self.dock_direction = aui_constants.AUI_DOCK_BOTTOM
        return self

    def Center(self):
        """
        Sets the pane to the center position of the frame.

        The centre pane is the space in the middle after all border panes
        (left, top, right, bottom) are subtracted from the layout.

        :note: This is the same thing as calling
        :meth:`~AuiPaneInfo.Direction` with ``AUI_DOCK_CENTER`` as parameter.
        """

        self.dock_direction = aui_constants.AUI_DOCK_CENTER
        return self

    def Centre(self):
        """
        Sets the pane to the center position of the frame.

        The centre pane is the space in the middle after all border panes
        (left, top, right, bottom) are subtracted from the layout.

        :note: This is the same thing as calling :meth:`~AuiPaneInfo.Direction`
        with ``aui_constants.AUI_DOCK_CENTRE`` as parameter.
        """

        self.dock_direction = aui_constants.AUI_DOCK_CENTRE
        return self

    def Direction(self, direction):
        """
        Determines the direction of the docked pane. It is functionally the
        same as calling :meth:`Left`, :meth:`Right`, :meth:`Top` or
        :meth:`Bottom`, except that docking direction may be specified
        programmatically via the parameter `direction`.

        :param integer `direction`: the direction of the docked pane.

        :see: :meth:`dock_direction_set` for a list of valid docking
        directions.
        """

        self.dock_direction = direction
        return self

    def Layer(self, layer):
        """
        Determines the layer of the docked pane.

        The dock layer is similar to an onion, the inner-most layer being
        layer 0. Each shell moving in the outward direction has a higher layer
        number. This allows for more complex docking layout formation.

        :param integer `layer`: the layer of the docked pane.
        """

        self.dock_layer = layer
        return self

    def Row(self, row):
        """
        Determines the row of the docked pane.

        :param integer `row`: the row of the docked pane.
        """

        self.dock_row = row
        return self

    def Position(self, pos):
        """
        Determines the position of the docked pane.

        :param integer `pos`: the position of the docked pane.
        """

        self.dock_pos = pos
        return self

    def MinSize(self, arg1=None, arg2=None):
        """
        Sets the minimum size of the pane.

        This method is split in 2 versions depending on the input type.
        If `arg1` is a :class:`Size` object, then :meth:`~AuiPaneInfo.MinSize1`
        is called. Otherwise, :meth:`~AuiPaneInfo.MinSize2` is called.

        :param `arg1`: a :class:`Size` object, a (x, y) tuple or or a `x`
        coordinate.
        :param `arg2`: a `y` coordinate (only if `arg1` is a `x` coordinate,
        otherwise unused).
        """

        if isinstance(arg1, wx.Size):
            ret = self.MinSize1(arg1)
        elif isinstance(arg1, tuple):
            ret = self.MinSize1(wx.Size(*arg1))
        elif isinstance(arg1, int) and arg2 is not None:
            ret = self.MinSize2(arg1, arg2)
        else:
            raise Exception("Invalid argument passed to `MinSize`: "
                            "arg1=%s, arg2=%s" % (repr(arg1), repr(arg2)))

        return ret

    def MinSize1(self, size):
        """
        Sets the minimum size of the pane.

        :see: :meth:`MinSize` for an explanation of input parameters.
        """
        self.min_size = size
        return self

    def MinSize2(self, x, y):
        """
        Sets the minimum size of the pane.

        :see: :meth:`MinSize` for an explanation of input parameters.
        """

        self.min_size = wx.Size(x, y)
        return self

    def MaxSize(self, arg1=None, arg2=None):
        """
        Sets the maximum size of the pane.

        This method is split in 2 versions depending on the input type. If
        `arg1` is a :class:`Size` object, then :meth:`~AuiPaneInfo.MaxSize1`
        is called. Otherwise, :meth:`~AuiPaneInfo.MaxSize2` is called.

        :param `arg1`: a :class:`Size` object, a (x, y) tuple or a `x`
         coordinate.
        :param `arg2`: a `y` coordinate (only if `arg1` is a `x` coordinate,
        otherwise unused).
        """

        if isinstance(arg1, wx.Size):
            ret = self.MaxSize1(arg1)
        elif isinstance(arg1, tuple):
            ret = self.MaxSize1(wx.Size(*arg1))
        elif isinstance(arg1, int) and arg2 is not None:
            ret = self.MaxSize2(arg1, arg2)
        else:
            raise Exception("Invalid argument passed to `MaxSize`: "
                            "arg1=%s, arg2=%s" % (repr(arg1), repr(arg2)))

        return ret

    def MaxSize1(self, size):
        """
        Sets the maximum size of the pane.

        :see: :meth:`MaxSize` for an explanation of input parameters.
        """

        self.max_size = size
        return self

    def MaxSize2(self, x, y):
        """
        Sets the maximum size of the pane.

        :see: :meth:`MaxSize` for an explanation of input parameters.
        """

        self.max_size.Set(x, y)
        return self

    def BestSize(self, arg1=None, arg2=None):
        """
        Sets the ideal size for the pane. The docking manager will attempt to
        use this size as much as possible when docking or floating the pane.

        This method is split in 2 versions depending on the input type. If
        `arg1` is a :class:`Size` object, then :meth:`BestSize1` is called.
        Otherwise, :meth:`BestSize2` is called.

        :param `arg1`: a :class:`Size` object, a (x, y) tuple or a `x`
         coordinate.
        :param `arg2`: a `y` coordinate (only if `arg1` is a `x` coordinate,
         otherwise unused).
        """

        if isinstance(arg1, wx.Size):
            ret = self.BestSize1(arg1)
        elif isinstance(arg1, tuple):
            ret = self.BestSize1(wx.Size(*arg1))
        elif isinstance(arg1, int) and arg2 is not None:
            ret = self.BestSize2(arg1, arg2)
        else:
            raise Exception("Invalid argument passed to `BestSize`: "
                            "arg1=%s,arg2=%s" % (repr(arg1), repr(arg2)))

        return ret

    def BestSize1(self, size):
        """
        Sets the best size of the pane.

        :see: :meth:`BestSize` for an explanation of input parameters.
        """

        self.best_size = size
        return self

    def BestSize2(self, x, y):
        """
        Sets the best size of the pane.

        :see: :meth:`BestSize` for an explanation of input parameters.
        """

        self.best_size.Set(x, y)
        return self

    def FloatingPosition(self, pos):
        """
        Sets the position of the floating pane.

        :param `pos`: a :class:`Point` or a tuple indicating the pane floating
        position.
        """

        self.floating_pos = wx.Point(*pos)
        return self

    def FloatingSize(self, size):
        """
        Sets the size of the floating pane.

        :param `size`: a :class:`Size` or a tuple indicating the pane floating
        size.
        """

        self.floating_size = wx.Size(*size)
        return self

    def Maximize(self):
        """ Makes the pane take up the full area."""

        return self.SetFlag(self.optionMaximized, True)

    def Minimize(self):
        """
        Makes the pane minimized in a :class:`~lib.agw.aui.auibar.AuiToolBar`.

        Clicking on the minimize button causes a new
        :class:`~lib.agw.aui.auibar.AuiToolBar` to be created and added to the
        frame manager, (currently the implementation is such that panes at West
        will have a toolbar at the right, panes at South will have toolbars at
        the bottom etc...) and the pane is hidden in the manager.

        Clicking on the restore button on the newly created toolbar will result
        in the toolbar being removed and the original pane being restored.
        """

        return self.SetFlag(self.optionMinimized, True)

    def MinimizeMode(self, mode):
        """
        Sets the expected minimized mode if the minimize button is visible.

        :param integer `mode`: the minimized pane can have a specific position
        in the work space:

        ============================== ========= ==============================
        Minimize Mode Flag             Hex Value Description
        ============================== ========= ==============================
        ``AUI_MINIMIZE_POS_SMART``          0x01 Minimizes the pane on the
                                                 closest tool bar
        ``AUI_MINIMIZE_POS_TOP``            0x02 Minimizes the pane on the
                                                 top tool bar
        ``AUI_MINIMIZE_POS_LEFT``           0x03 Minimizes the pane on its
                                                 left tool bar
        ``AUI_MINIMIZE_POS_RIGHT``          0x04 Minimizes the pane on its
                                                 right tool bar
        ``AUI_MINIMIZE_POS_BOTTOM``         0x05 Minimizes the pane on its
                                                 bottom tool bar
        ``AUI_MINIMIZE_POS_TOOLBAR``        0x06 Minimizes the pane on a target
                                                 :class:`~lib.agw.aui.auibar.
                                                 AuiToolBar`
        ============================== ========= ==============================

        The caption of the minimized pane can be displayed in different modes:

        ============================== ========= ==============================
        Caption Mode Flag              Hex Value Description
        ============================== ========= ==============================
        ``AUI_MINIMIZE_CAPT_HIDE``           0x0 Hides the caption of the
                                                 minimized pane
        ``AUI_MINIMIZE_CAPT_SMART``         0x08 Displays the caption in the
                                                 best rotation (horizontal in
                                                 the top and in the bottom tool
                                                 bar or clockwise in the right
                                                 and in the left tool bar)
        ``AUI_MINIMIZE_CAPT_HORZ``          0x10 Displays the caption
                                                 horizontally
        ============================== ========= ==============================

        .. note::

           In order to use the ``AUI_MINIMIZE_POS_TOOLBAR`` flag, the instance
           of :class:`AuiManager` you pass as an input for
           :meth:`MinimizeTarget` **must** have a real name and not the
           randomly generated one. Remember to set the :meth:`Name` property
           of the toolbar pane before calling this method.

        """

        self.minimize_mode = mode
        return self

    def MinimizeTarget(self, toolbarPane):
        """
        Minimizes the panes using a :class:`AuiPaneInfo` as a target. As
        :class:`AuiPaneInfo` properties need to be copied back and forth every
        time the perspective has changed, we only store the toobar **name**.

        :param `toolbarPane`: an instance of :class:`AuiPaneInfo`, containing
        a :class:`~lib.agw.aui.auibar.AuiToolBar`.

        .. note::

           In order to use this functionality (and with the
           ``AUI_MINIMIZE_POS_TOOLBAR`` flag set), the instance of
           :class:`AuiPaneInfo` you pass as an input **must** have a real
           name and not the randomly generated one. Remember to set the
           :meth:`Name` property of
           the toolbar pane before calling this method.

        """

        self.minimize_target = toolbarPane.name
        return self

    def Restore(self):
        """ Is the reverse of :meth:`Maximize` and :meth:`Minimize`."""

        return self.SetFlag(self.optionMaximized or self.optionMinimized,
                            False)

    def Fixed(self):
        """
        Forces a pane to be fixed size so that it cannot be resized.
        After calling :meth:`Fixed`, :meth:`IsFixed` will return ``True``.
        """

        return self.SetFlag(self.optionResizable, False)

    def Resizable(self, resizable=True):
        """
        Allows a pane to be resizable if `resizable` is ``True``, and forces
        it to be a fixed size if `resizeable` is ``False``.

        If `resizable` is ``False``, this is simply an antonym for
        :meth:`Fixed`.

        :param bool `resizable`: whether the pane will be resizeable or not.
        """

        return self.SetFlag(self.optionResizable, resizable)

    def Transparent(self, alpha):
        """
        Makes the pane transparent when floating.

        :param integer `alpha`: a value between 0 and 255 for pane
        transparency.
        """

        if alpha < 0 or alpha > 255:
            raise Exception("Invalid transparency value (%s)" % repr(alpha))

        self.transparent = alpha
        self.needsTransparency = True

    def Dock(self):
        """ Indicates that a pane should be docked. It is the
        opposite of :meth:`Float`. """

        if self.IsNotebookPage():
            self.notebook_id = -1
            self.dock_direction = aui_constants.AUI_DOCK_NONE

        return self.SetFlag(self.optionFloating, False)

    def Float(self):
        """ Indicates that a pane should be floated. It is the opposite
        of :meth:`Dock`. """

        if self.IsNotebookPage():
            self.notebook_id = -1
            self.dock_direction = aui_constants.AUI_DOCK_NONE

        return self.SetFlag(self.optionFloating, True)

    def Hide(self):
        """
        Indicates that a pane should be hidden.

        Calling :meth:`Show(False) <Show>` achieve the same effect.
        """

        return self.SetFlag(self.optionHidden, True)

    def Show(self, show=True):
        """
        Indicates that a pane should be shown.

        :param bool `show`: whether the pane should be shown or not.
        """

        return self.SetFlag(self.optionHidden, not show)

    # By defaulting to 1000, the tab will get placed at the end
    def NotebookPage(self, id, tab_position=1000):
        """
        Forces a pane to be a notebook page, so that the pane can be
        docked on top to another to create a
        :class:`~lib.agw.aui.auibook.AuiNotebook`.

        :param integer `id`: the notebook id;
        :param integer `tab_position`: the tab number of the pane once
        docked in a notebook.
        """

        # Remove any floating frame
        self.Dock()
        self.notebook_id = id
        self.dock_pos = tab_position
        self.dock_row = 0
        self.dock_layer = 0
        self.dock_direction = aui_constants.AUI_DOCK_NOTEBOOK_PAGE

        return self

    def NotebookControl(self, id):
        """
        Forces a pane to be a notebook control
        (:class:`~lib.agw.aui.auibook.AuiNotebook`).

        :param integer `id`: the notebook id.
        """

        self.notebook_id = id
        self.window = None
        self.buttons = []

        if self.dock_direction == aui_constants.AUI_DOCK_NOTEBOOK_PAGE:
            self.dock_direction = aui_constants.AUI_DOCK_NONE

        return self

    def HasNotebook(self):
        """ Returns whether a pane has a
        :class:`~lib.agw.aui.auibook.AuiNotebook` or not. """

        return self.notebook_id >= 0

    def IsNotebookPage(self):
        """ Returns whether the pane is a notebook page in a
        :class:`~lib.agw.aui.auibook.AuiNotebook`. """

        return self.notebook_id >= 0 and \
            self.dock_direction == aui_constants.AUI_DOCK_NOTEBOOK_PAGE

    def IsNotebookControl(self):
        """ Returns whether the pane is a notebook control
        (:class:`~lib.agw.aui.auibook.AuiNotebook`). """

        return not self.IsNotebookPage() and self.HasNotebook()

    def SetNameFromNotebookId(self):
        """ Sets the pane name once docked in a
        :class:`~lib.agw.aui.auibook.AuiNotebook` using the notebook id. """

        if self.notebook_id >= 0:
            self.name = "__notebook_%d" % self.notebook_id

        return self

    def CaptionVisible(self, visible=True, left=False):
        """
        Indicates that a pane caption should be visible. If `visible` is
        ``False``, no pane caption is drawn.

        :param bool `visible`: whether the caption should be visible or not;
        :param bool `left`: whether the caption should be drawn on the
        left (rotated by 90 degrees) or not.
        """

        if left:
            self.SetFlag(self.optionCaption, False)
            return self.SetFlag(self.optionCaptionLeft, visible)

        self.SetFlag(self.optionCaptionLeft, False)
        return self.SetFlag(self.optionCaption, visible)

    def PaneBorder(self, visible=True):
        """
        Indicates that a border should be drawn for the pane.

        :param bool `visible`: whether the pane border should be visible
        or not.
        """

        return self.SetFlag(self.optionPaneBorder, visible)

    def Gripper(self, visible=True):
        """
        Indicates that a gripper should be drawn for the pane.

        :param bool `visible`: whether the gripper should be visible or not.
        """

        return self.SetFlag(self.optionGripper, visible)

    def GripperTop(self, attop=True):
        """
        Indicates that a gripper should be drawn at the top of the pane.

        :param bool `attop`: whether the gripper should be drawn at the
        top or not.
        """

        return self.SetFlag(self.optionGripperTop, attop)

    def CloseButton(self, visible=True):
        """
        Indicates that a close button should be drawn for the pane.

        :param bool `visible`: whether the close button should be
        visible or not.
        """

        return self.SetFlag(self.buttonClose, visible)

    def MaximizeButton(self, visible=True):
        """
        Indicates that a maximize button should be drawn for the pane.

        :param bool `visible`: whether the maximize button should be
        visible or not.
        """

        return self.SetFlag(self.buttonMaximize, visible)

    def MinimizeButton(self, visible=True):
        """
        Indicates that a minimize button should be drawn for the pane.

        :param bool `visible`: whether the minimize button should be
         visible or not.
        """

        return self.SetFlag(self.buttonMinimize, visible)

    def PinButton(self, visible=True):
        """
        Indicates that a pin button should be drawn for the pane.

        :param bool `visible`: whether the pin button should be visible or not.
        """

        return self.SetFlag(self.buttonPin, visible)

    def DestroyOnClose(self, b=True):
        """
        Indicates whether a pane should be destroyed when it is closed.

        Normally a pane is simply hidden when the close button is clicked.
        Setting `b` to ``True`` will cause the window to be destroyed when
        the user clicks the pane's close button.

        :param bool `b`: whether the pane should be destroyed when it is
        closed or not.
        """

        return self.SetFlag(self.optionDestroyOnClose, b)

    def TopDockable(self, b=True):
        """
        Indicates whether a pane can be docked at the top of the frame.

        :param bool `b`: whether the pane can be docked at the top or not.
        """

        return self.SetFlag(self.optionTopDockable, b)

    def BottomDockable(self, b=True):
        """
        Indicates whether a pane can be docked at the bottom of the frame.

        :param bool `b`: whether the pane can be docked at the bottom or not.
        """

        return self.SetFlag(self.optionBottomDockable, b)

    def LeftDockable(self, b=True):
        """
        Indicates whether a pane can be docked on the left of the frame.

        :param bool `b`: whether the pane can be docked at the left or not.
        """

        return self.SetFlag(self.optionLeftDockable, b)

    def RightDockable(self, b=True):
        """
        Indicates whether a pane can be docked on the right of the frame.

        :param bool `b`: whether the pane can be docked at the right or not.
        """

        return self.SetFlag(self.optionRightDockable, b)

    def Floatable(self, b=True):
        """
        Sets whether the user will be able to undock a pane and turn it
        into a floating window.

        :param bool `b`: whether the pane can be floated or not.
        """

        return self.SetFlag(self.optionFloatable, b)

    def Movable(self, b=True):
        """
        Indicates whether a pane can be moved.

        :param bool `b`: whether the pane can be moved or not.
        """

        return self.SetFlag(self.optionMovable, b)

    def NotebookDockable(self, b=True):
        """
        Indicates whether a pane can be docked in an automatic
        :class:`~lib.agw.aui.auibook.AuiNotebook`.

        :param bool `b`: whether the pane can be docked in a notebook or not.
        """

        return self.SetFlag(self.optionNotebookDockable, b)

    def DockFixed(self, b=True):
        """
        Causes the containing dock to have no resize sash. This is useful
        for creating panes that span the entire width or height of a dock,
        but should not be resizable in the other direction.

        :param bool `b`: whether the pane will have a resize sash or not.
        """

        return self.SetFlag(self.optionDockFixed, b)

    def Dockable(self, b=True):
        """
        Specifies whether a frame can be docked or not. It is the same as
        specifying :meth:`TopDockable` . :meth:`BottomDockable` .
        :meth:`LeftDockable` . :meth:`RightDockable` .

        :param bool `b`: whether the frame can be docked or not.
        """

        return self.TopDockable(b).BottomDockable(b).LeftDockable(b).\
            RightDockable(b)

    def TopSnappable(self, b=True):
        """
        Indicates whether a pane can be snapped at the top of the main frame.

        :param bool `b`: whether the pane can be snapped at the top of the
        main frame or not.
        """

        return self.SetFlag(self.optionTopSnapped, b)

    def BottomSnappable(self, b=True):
        """
        Indicates whether a pane can be snapped at the bottom of the main i
        frame.

        :param bool `b`: whether the pane can be snapped at the bottom of the
        main frame or not.
        """

        return self.SetFlag(self.optionBottomSnapped, b)

    def LeftSnappable(self, b=True):
        """
        Indicates whether a pane can be snapped on the left of the main frame.

        :param bool `b`: whether the pane can be snapped at the left of the
        main frame or not.
        """

        return self.SetFlag(self.optionLeftSnapped, b)

    def RightSnappable(self, b=True):
        """
        Indicates whether a pane can be snapped on the right of the main frame.

        :param bool `b`: whether the pane can be snapped at the right of the
        main frame or not.
        """

        return self.SetFlag(self.optionRightSnapped, b)

    def Snappable(self, b=True):
        """
        Indicates whether a pane can be snapped on the main frame. This is
        equivalent as calling :meth:`TopSnappable` . :meth:`BottomSnappable` .
        :meth:`LeftSnappable` . :meth:`RightSnappable` .

        :param bool `b`: whether the pane can be snapped on the main frame
        or not.
        """

        return self.TopSnappable(b).BottomSnappable(b).LeftSnappable(b).\
            RightSnappable(b)

    def FlyOut(self, b=True):
        """
        Indicates whether a pane, when floating, has a "fly-out" effect
        (i.e., floating panes which only show themselves when moused over).

        :param bool `b`: whether the pane can be snapped on the main frame
        or not.
        """

        return self.SetFlag(self.optionFlyOut, b)

    # Copy over the members that pertain to docking position
    def SetDockPos(self, source):
        """
        Copies the `source` pane members that pertain to docking position to
        `self`.

        :param AuiPaneInfo `source`: the source pane from where to copy the
        attributes.
        """

        self.dock_direction = source.dock_direction
        self.dock_layer = source.dock_layer
        self.dock_row = source.dock_row
        self.dock_pos = source.dock_pos
        self.dock_proportion = source.dock_proportion
        self.floating_pos = wx.Point(*source.floating_pos)
        self.floating_size = wx.Size(*source.floating_size)
        self.rect = wx.Rect(*source.rect)

        return self

    def DefaultPane(self):
        """ Specifies that the pane should adopt the default pane settings. """

        state = self.state
        state |= self.optionTopDockable | self.optionBottomDockable | \
            self.optionLeftDockable | self.optionRightDockable | \
            self.optionNotebookDockable | self.optionFloatable | \
            self.optionMovable | self.optionResizable | \
            self.optionCaption | self.optionPaneBorder | self.buttonClose

        self.state = state
        return self

    def CentrePane(self):
        """
        Specifies that the pane should adopt the default center pane settings.

        Centre panes usually do not have caption bars. This function provides
        an easy way of preparing a pane to be displayed
        in the center dock position.
        """

        return self.CenterPane()

    def CenterPane(self):
        """
        Specifies that the pane should adopt the default center pane settings.

        Centre panes usually do not have caption bars. This function provides
        an easy way of preparing a pane to be displayed in the center dock
        position.
        """

        self.state = 0
        return self.Center().PaneBorder().Resizable()

    def ToolbarPane(self):
        """ Specifies that the pane should adopt the default toolbar pane
        settings. """

        self.DefaultPane()
        state = self.state

        state |= (self.optionToolbar | self.optionGripper)
        state &= ~(self.optionResizable | self.optionCaption |
                   self.optionCaptionLeft)

        if self.dock_layer == 0:
            self.dock_layer = 10

        self.state = state

        return self

    def Icon(self, icon):
        """
        Specifies whether an icon is drawn on the left of the caption text when
        the pane is docked. If `icon` is ``None`` or :class:`NullIcon`,
        no icon is drawn on the caption space.

        :param icon: an icon to draw on the caption space, or ``None``.
        :type `icon`: :class:`Icon` or ``None``
        """

        if icon is None:
            icon = wx.NullIcon

        self.icon = icon
        return self

    def SetFlag(self, flag, option_state):
        """
        Turns the property given by `flag` on or off with the `option_state`
        parameter.

        :param integer `flag`: the property to set;
        :param bool `option_state`: either ``True`` or ``False``.
        """

        state = self.state

        if option_state:
            state |= flag
        else:
            state &= ~flag

        self.state = state

        if flag in [self.buttonClose, self.buttonMaximize,
                    self.buttonMinimize, self.buttonPin]:
            self.ResetButtons()

        return self

    def HasFlag(self, flag):
        """
        Returns ``True`` if the the property specified by flag is active
        for the pane.

        :param integer `flag`: the property to check for activity.
        """

        return (self.state & flag and [True] or [False])[0]

    def ResetButtons(self):
        """
        Resets all the buttons and recreates them from scratch depending on the
        :class:`AuiManager` flags.
        """

        floating = self.HasFlag(self.optionFloating)
        self.buttons = []

        if not floating and self.HasMinimizeButton():
            button = AuiPaneButton(aui_constants.AUI_BUTTON_MINIMIZE)
            self.buttons.append(button)

        if not floating and self.HasMaximizeButton():
            button = AuiPaneButton(aui_constants.AUI_BUTTON_MAXIMIZE_RESTORE)
            self.buttons.append(button)

        if not floating and self.HasPinButton():
            button = AuiPaneButton(aui_constants.AUI_BUTTON_PIN)
            self.buttons.append(button)

        if self.HasCloseButton():
            button = AuiPaneButton(aui_constants.AUI_BUTTON_CLOSE)
            self.buttons.append(button)

    def CountButtons(self):
        """ Returns the number of visible buttons in the docked pane. """

        n = 0

        if self.HasCaption() or self.HasCaptionLeft():
            if isinstance(wx.GetTopLevelParent(self.window), wx.MiniFrame):
                return 1

            if self.HasCloseButton():
                n += 1
            if self.HasMaximizeButton():
                n += 1
            if self.HasMinimizeButton():
                n += 1
            if self.HasPinButton():
                n += 1

        return n

    def IsHorizontal(self):
        """ Returns ``True`` if the pane `dock_direction` is horizontal. """

        return self.dock_direction in [aui_constants.AUI_DOCK_TOP,
                                       aui_constants.AUI_DOCK_BOTTOM]

    def IsVertical(self):
        """ Returns ``True`` if the pane `dock_direction` is vertical. """

        return self.dock_direction in [aui_constants.AUI_DOCK_LEFT,
                                       aui_constants.AUI_DOCK_RIGHT]


# Null AuiPaneInfo reference
NonePaneInfo = AuiPaneInfo()
""" Null :class:`AuiPaneInfo` reference, an invalid instance of
:class:`AuiPaneInfo`. """


class AuiPaneButton(object):
    """ A simple class which describes the caption pane button attributes. """

    def __init__(self, button_id):
        """
        Default class constructor.
        Used internally, do not call it in your code!

        :param integer `button_id`: the pane button identifier.
        """

        self.button_id = button_id
